#include <boost/bind.hpp>
#include <algorithm>
#include <memory>
#include <chrono>
#include "clientset.hpp"

#include "udpserver.hpp"

const std::string UdpServer::CLIENT = "CLIENT ";
const std::string UdpServer::UPLOAD = "UPLOAD ";
const std::string UdpServer::RETRANSMIT = "RETRANSMIT ";
const std::string UdpServer::KEEPALIVE = "KEEPALIVE\n";

UdpServer::UdpServer(boost::asio::io_service& io, ClientSet& clients,
    uint16_t port, unsigned int interval):
socket(io, boost::asio::ip::udp::endpoint(boost::asio::ip::udp::v6(), port)),
clients(clients), mixerTimer(io), interval(interval)
{
    //setupSocket(io, port);
    clients.setUdpSocket(&socket);
    startReceive();
    startMixer();
}

void UdpServer::setupSocket(boost::asio::io_service& io, uint16_t port)
{
    boost::asio::ip::udp::resolver resolver(io);
    boost::asio::ip::udp::resolver::query query(std::to_string(port));
    boost::asio::ip::udp::resolver::iterator endpointIt =
        resolver.resolve(query);
    boost::asio::connect(socket, endpointIt);
}

UdpServer::~UdpServer()
{
    try {
        socket.shutdown(boost::asio::ip::udp::socket::shutdown_both);
        socket.close();
    } catch (boost::system::system_error& e) {}
}

void UdpServer::startReceive()
{
    //if (debug) std::cerr << "(UDP) startReceive\n";

    socket.async_receive_from(boost::asio::buffer(buffer), remoteEndpoint,
        boost::bind(&UdpServer::handleReceive, this,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
}

void UdpServer::handleReceive(const::boost::system::error_code& error,
    size_t bytes_transferred)
{
    if (debug) std::cerr << "(UDP) error code: " << error << "\n";
    if (debug) std::cerr << "(UDP) bytes transferred = " <<
                    bytes_transferred << "\n";
    if (debug) std::cerr << "(UDP) buffer: \n";
    //if (debug) std::cerr.write(buffer, bytes_transferred);

    if (error) {
        if (debug) std::cerr << "(UDP) Error: " << error << "\n";
        startReceive();
        return;
    }

    if (bytes_transferred == 0) {
        if (debug) std::cerr << "(UDP) bytes transferred = 0\n";
        startReceive();
        return;
    }

    if (CLIENT.compare(0, CLIENT.size(), buffer, CLIENT.size()) == 0) {
        setupClient(bytes_transferred);
    } else if (UPLOAD.compare(0, UPLOAD.size(), buffer, UPLOAD.size()) == 0) {
        upload(bytes_transferred);
    } else if (RETRANSMIT.compare(
                0, RETRANSMIT.size(), buffer, RETRANSMIT.size()) == 0) {
        retransmit(bytes_transferred);
    } else if (KEEPALIVE.compare(
                0, KEEPALIVE.size(), buffer, KEEPALIVE.size()) == 0) {
        keepalive();
    } else {
        std::cerr << "Niezrozumialy komunikat (UDP):" << std::endl;
        std::cerr.write(buffer, bytes_transferred);
    }

    startReceive();
}

void UdpServer::setupClient(size_t size)
{
    try {
        if (debug) std::cerr << "UDP: ";
        if (debug) std::cerr.write(buffer, size);

        char* endline = std::find(buffer + CLIENT.size(), buffer + size, '\n');
        if (endline == buffer + size) {
            if (debug) std::cerr << "Niezrozumialy komunikat (UDP)" << std::endl;
            return;
        }
        unsigned long id = atoi(buffer + CLIENT.size());

        clients.update(id, remoteEndpoint);
    } catch (const std::out_of_range& e) {
        if (debug) std::cerr << "nie udalo sie" << std::endl;
    }
}

void UdpServer::upload(size_t size)
{
    try {
        if (debug) std::cerr << "UDP: ";
        if (debug) std::cerr.write(buffer, 10);

        Client::pointer c = clients.get(remoteEndpoint);

        long dataNo = atoi(buffer + UPLOAD.size());
        char* endline = std::find(buffer, buffer + size, '\n');

        if (endline == buffer + size) {
            if (debug) std::cerr << "Niezrozumialy komunikat (UDP)" << std::endl;
            clients.remove(c);
            return;
        }

        size_t rawDataSize = size - (endline + 1 - buffer);

        if (rawDataSize > c->getSpace()) {
            clients.remove(c);
            return;
        }

        char* rawData = endline + 1;
        c->upload(dataNo, rawData, rawDataSize);
    } catch (const std::out_of_range& e) {}
}

void UdpServer::retransmit(size_t size)
{
    try {
        if (debug) std::cerr << "UDP: ";
        if (debug) std::cerr.write(buffer, size);

        Client::pointer c = clients.get(remoteEndpoint);

        char* endline = std::find(buffer + CLIENT.size(), buffer + size, '\n');
        if (endline == buffer + size) {
            if (debug) std::cerr << "Niezrozumialy komunikat (UDP)" << std::endl;
            clients.remove(c);
            return;
        }

        long dataNo = atoi(buffer + RETRANSMIT.size());

        clients.retransmit(c, dataNo);
    } catch (const std::out_of_range& e) {}
}

void UdpServer::keepalive()
{
    try {
        clients.get(remoteEndpoint)->keepalive();
    } catch (const std::out_of_range& e) {}
}

void UdpServer::startMixer()
{
    mixerTimer.expires_at(
        std::chrono::steady_clock::now() + std::chrono::milliseconds(interval));
    mixerTimer.async_wait(boost::bind(&UdpServer::startMixer, this));

    clients.runMixer(interval);
}
