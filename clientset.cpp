#include <sstream>

#include "serverclient.hpp"
#include "mixer.hpp"

#include "clientset.hpp"

ClientSet::ClientSet(size_t fifoSize, size_t fifoLo, size_t fifoHi,
    size_t bufferLength):
fifoSize(fifoSize), fifoLo(fifoLo), fifoHi(fifoHi), bufferLength(bufferLength),
udpSocket(NULL), sentDataNo(0), sentDataBuffer()
{
}


ClientSet::~ClientSet()
{
    while (!sentDataBuffer.empty()) {
        delete[] sentDataBuffer.front().first;
        sentDataBuffer.pop_front();
    }

}

void ClientSet::add(unsigned long id, Client::pointer client)
{
    clientIdMap[id] = client;
}

void ClientSet::update(unsigned long id, boost::asio::ip::udp::endpoint& endpoint)
{
    Client::pointer client;
    client = clientIdMap.at(id);
    clientUdpMap[endpoint] = client;
    client->setUdpEndpoint(endpoint);
}

Client::pointer ClientSet::get(boost::asio::ip::udp::endpoint& endpoint)
{
    return clientUdpMap.at(endpoint);
}

void ClientSet::remove(Client::pointer client)
{
    clientIdMap.erase(client->getId());
    clientUdpMap.erase(client->getUdpEndpoint());
}

std::string ClientSet::getReport()
{
    std::stringstream ss;
    ss << "\n";

    for (auto& el : clientUdpMap) {
        Client::pointer c = el.second;
        try {
            size_t min, max;
            c->getStats(min, max);
            auto endpoint = c->getTcpSocket().remote_endpoint();
            ss << endpoint.address().to_string() << ":" << endpoint.port() <<
                  " FIFO: " << c->getFifoSize() << "/" << fifoSize <<
                  " (min. " << min << ", max. " << max << ")\n";
        } catch (std::exception& e) {
            if (debug) std::cerr << "Zlapany wyjatek przy tworzeniu " <<
                        "raportu: " << e.what() << std::endl;
        }
    }

    return ss.str();
}

size_t ClientSet::getFifoSize()
{
    return fifoSize;
}

size_t ClientSet::getFifoLowWatermark()
{
    return fifoLo;
}

size_t ClientSet::getFifoHighWatermark()
{
    return fifoHi;
}

void ClientSet::setUdpSocket(boost::asio::ip::udp::socket* socket)
{
    udpSocket = socket;
}

boost::asio::ip::udp::socket& ClientSet::getUdpSocket()
{
    return *udpSocket;
}

void ClientSet::collectData(mixer_input* inputs)
{
    size_t i = 0;
    for (auto& el: clientIdMap) {
        Client::pointer c = el.second;
        c->fillMixerInput(inputs[i].data, inputs[i].len);
        ++i;
    }
}

void ClientSet::retransmit(Client::pointer client, unsigned long datagramNo)
{
    size_t offset;
    if (sentDataNo + 1 > datagramNo + bufferLength)
        offset = 0;
    else
        offset = datagramNo - sentDataNo + bufferLength - 1;

    for ( ; offset < sentDataBuffer.size(); ++offset) {
        client->sendData(
            sentDataBuffer[offset].first,
            sentDataBuffer[offset].second,
            sentDataNo - sentDataBuffer.size() + 1 + offset);
    }
}

void ClientSet::sendData(void* outputBuffer, size_t size)
{
    for (auto& el : clientUdpMap) {
        Client::pointer c = el.second;
        c->sendData((char*)outputBuffer, size, sentDataNo);
    }
    ++sentDataNo;
}

void ClientSet::updateFifo(mixer_input* inputs)
{
    size_t i = 0;
    for (auto& el : clientIdMap) {
        Client::pointer c = el.second;
        c->updateFifo(inputs[i].consumed);
        ++i;
    }
}

void ClientSet::runMixer(unsigned int interval)
{
    size_t clientCount = clientIdMap.size();

    if (clientCount == 0)
        return;

    mixer_input* inputs = new mixer_input[clientCount];
    size_t outputSize = DATA_M * interval;
    void* outputBuffer = (void*) (new char[outputSize]);

    collectData(inputs);

    mixer(inputs, clientCount, outputBuffer, &outputSize, interval);

    updateFifo(inputs);
    updateMixerBuffer(outputBuffer, outputSize);
    sendData(outputBuffer, outputSize);

    delete[] inputs;
    inputs = NULL;
}

void ClientSet::updateMixerBuffer(void* outputBuffer, size_t outputSize)
{
    sentDataBuffer.push_back(std::make_pair((char*)outputBuffer, outputSize));
    if (sentDataBuffer.size() > bufferLength) {
        delete[] sentDataBuffer.front().first;
        sentDataBuffer.pop_front();
    }
}

