#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

#ifdef DEBUG
const bool debug = true;
#else
const bool debug = false;
#endif

const uint16_t PORT_DEFAULT = 16699;
const size_t FIFO_SIZE_DEFAULT = 10560;
const size_t FIFO_LO_MARK_DEFAULT = 0;
const size_t FIFO_HI_MARK_DEFAULT = FIFO_SIZE_DEFAULT;
const size_t BUF_LEN_DEFAULT = 10;
const size_t RETR_LIMIT_DEFAULT = 10;
const unsigned int INTERVAL_DEFAULT = 5;

const size_t UDP_BUFFER_SIZE = 64 * 1024;
const size_t TCP_BUFFER_SIZE = 8 * 1024;
const size_t STDIN_BUFFER_SIZE = 63 * 1024;
const size_t DATA_M = 176;
const size_t CONN_CHECK_FREQ = 1000; /*ms*/
const size_t REPORT_FREQ = 1000; /*ms*/
const size_t RECONNECT_FREQ = 1000; /*ms*/
const size_t KEEPALIVE_FREQ = 100; /*ms*/


#endif
