#ifndef SERVERINDATA_H
#define SERVERINDATA_H

#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>
#include <memory>
#include <chrono>
#include <cstdint>
#include <cstdlib>

#include "constants.hpp"

class ClientSet;
class TcpServer;

class Client: public std::enable_shared_from_this<Client>
{
public:
    typedef std::shared_ptr<Client> pointer;

    enum States {
        FILLING,
        ACTIVE
    };

    static pointer create(
        boost::asio::io_service& io,
        ClientSet& clients,
        unsigned long id,
        std::shared_ptr<boost::asio::ip::tcp::socket> tcpSocket);

    unsigned long getId();
    boost::asio::ip::tcp::socket& getTcpSocket();
    boost::asio::ip::udp::endpoint& getUdpEndpoint();
    void setUdpEndpoint(boost::asio::ip::udp::endpoint& endpoint);
    void keepalive();
    void upload(long dataNo, char* data, size_t size);
    size_t getSpace();
    size_t getFifoSize();
    void getStats(size_t& min, size_t& max);
    bool isActive();
    void updateFifo(size_t consumed);
    void fillMixerInput(void*& data, size_t& size);
    void sendData(char* data, size_t size, long dataNo);

    void start();
    virtual ~Client();

private:
    ClientSet& clients;
    unsigned long id;
    /** Kolejka FIFO.
     */
    char* queue;
    /** Status kolejki FIFO (FILLING/ACTIVE).
     */
    States qState;
    /** Liczba zajetych bajtow w kolejce FIFO.
     */
    size_t qSize;
    size_t qSizeMin;
    size_t qSizeMax;
    /** Numer ostatnio otrzymanego datagramu.
     */
    long receivedDataNo;
    std::shared_ptr<boost::asio::ip::tcp::socket> tcpSocket;
    boost::asio::ip::udp::endpoint udpEndpoint;
    boost::asio::steady_timer reportTimer;

    void sendId();
    void handleSendId(const boost::system::error_code& error, size_t);
    void sendReport();
    void handleSendReport(const boost::system::error_code& error, size_t);
    void updateState();
    void sendAck();
    void sendMessage(std::shared_ptr<std::string> message);
    void handleSend(const boost::system::error_code& error);
    void sendBuffer(char* buffer, size_t size);
    void handleSendBuffer(const boost::system::error_code& error, char* buffer);
    void updateStats();
    Client(
        boost::asio::io_service& io,
        ClientSet& clients,
        unsigned long id,
        std::shared_ptr<boost::asio::ip::tcp::socket> tcpSocket);
};

#endif /* SERVERINDATA_H */
