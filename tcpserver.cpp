#include <boost/bind.hpp>
#include "serverclient.hpp"
#include "clientset.hpp"

#include "tcpserver.hpp"

unsigned long TcpServer::clientCount = 0;

TcpServer::TcpServer(boost::asio::io_service& io, ClientSet& clients,
    uint16_t port):
clients(clients),
io_service(io),
acceptor(io, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v6(), port)),
port(port)
{
    startAccept();

}

TcpServer::~TcpServer()
{

}

void TcpServer::startAccept()
{
    if (debug) std::cerr << "Nasluchuje na tcp" << std::endl;

    std::shared_ptr<boost::asio::ip::tcp::socket> socket(
        new boost::asio::ip::tcp::socket(io_service));

    acceptor.async_accept(*socket,
        boost::bind(
            &TcpServer::handleAccept,
            this,
            boost::asio::placeholders::error,
            socket
        )
    );
}

void TcpServer::handleAccept(const boost::system::error_code& error,
    std::shared_ptr<boost::asio::ip::tcp::socket> tcpSocket)
{
    if (!error) { /* success = 0 */
        if (debug) std::cerr << "Tworze obiekt klienta" << std::endl;
        Client::pointer client =
             Client::create(io_service, clients, clientCount, tcpSocket);
        clients.add(clientCount, client);
        client->start();
        ++clientCount;
    }

    startAccept();
}

uint16_t TcpServer::getPort()
{
    return port;
}
