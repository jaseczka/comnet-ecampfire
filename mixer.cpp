#include <iostream>
#include <cstdint>
#include <algorithm>
#include "mixer.hpp"

void addInput(int16_t* output, size_t size, mixer_input& input)
{
    size_t i = 0;
    while(i < size && i < input.len / 2) {
        long val = (long) output[i] + ((int16_t*) input.data)[i];
        if (val > INT16_MAX)
            output[i] = INT16_MAX;
        else if (val < INT16_MIN)
            output[i] = INT16_MIN;
        else
            output[i] = (int16_t) val;
        ++i;
    }
    input.consumed = 2 * i;
}

void mixer(
    struct mixer_input* inputs, size_t n,
    void* output_buf, size_t* output_size,
    unsigned long /*tx_interval_ms*/
)
{
    if (output_size == 0)
        return;

    int16_t* output = (int16_t*) output_buf;
    std::fill(output, output + *output_size / 2, 0);

    size_t mixedData = 0;
    for (size_t i = 0; i < n; ++i) {
        addInput(output, *output_size / 2, inputs[i]);
        if (inputs[i].consumed > mixedData)
            mixedData = inputs[i].consumed;
    }

    *output_size = mixedData;
}


