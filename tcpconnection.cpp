#include <boost/bind.hpp>
#include <iostream>

#include "udpconnection.hpp"

#include "tcpconnection.hpp"

TcpConnection::TcpConnection(boost::asio::io_service& io,
    std::string& server, uint16_t port, UdpConnection& udpConnection):
udpConnection(udpConnection),
io(io), server(server), port(port), socket(io),
connectTimer(io), healthy(true)
{
    run();
}


TcpConnection::~TcpConnection()
{
    try {
        socket.shutdown(boost::asio::ip::tcp::socket::shutdown_both);
        socket.close();
    } catch (boost::system::system_error& e) {}
}

void TcpConnection::run()
{
    try {
        connect();
        startReceive();
    } catch (boost::system::system_error& e) {
        if (debug) std::cerr << "(TCP) run exception" << std::endl;
        connectTimer.expires_from_now(
            std::chrono::milliseconds(RECONNECT_FREQ));
        connectTimer.wait();
        run();
    }
}

void TcpConnection::connect()
{
    boost::system::error_code error;
    boost::asio::ip::tcp::resolver resolver(io);
    boost::asio::ip::tcp::resolver::query query(server,
        std::to_string(port));
    boost::asio::ip::tcp::resolver::iterator endpointIt =
        resolver.resolve(query, error);

    boost::asio::connect(socket, endpointIt);

}

void TcpConnection::startReceive()
{

    socket.async_receive(boost::asio::buffer(buffer),
        boost::bind(&TcpConnection::handleReceive, this,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
}

void TcpConnection::handleReceive(const boost::system::error_code& error,
    size_t bytes_transferred)
{
    if (error == boost::asio::error::operation_aborted)
        return;

    if (error) {
        if (debug) std::cerr << "(TCP) handleReceive healthy = false; " <<
                                    "message: " << error.message() << std::endl;
        healthy = false;
        return;
    }

    static std::string CLIENT = "CLIENT ";

    if (bytes_transferred == 0)
        return;
    else if (CLIENT.compare(0, CLIENT.size(), buffer, CLIENT.size()) == 0)
        sendId(bytes_transferred);
    else if (buffer[0] == '\n')
        handleReport(bytes_transferred);
    else
        std::cerr << "Niezrozumialy komunikat (TCP)" << std::endl;

    startReceive();
}

void TcpConnection::sendId(size_t size)
{
    std::shared_ptr<std::string> message(new std::string(buffer, size));
    udpConnection.sendMessage(message);
}

void TcpConnection::handleReport(size_t bytes_transferred)
{
    if (debug) std::cerr.write(buffer, bytes_transferred);
}

void TcpConnection::restart()
{
    try {
        socket.shutdown(boost::asio::ip::tcp::socket::shutdown_both);
        socket.close();
    } catch (boost::system::system_error& e) {}
    run();
    healthy = true;
    if (debug) std::cerr << "(TCP) restart healthy = true" << std::endl;
}

bool TcpConnection::isHealthy()
{
    return healthy;
}
