#include <boost/bind.hpp>
#include <thread>
#include <sstream>

#include "clientset.hpp"

#include "serverclient.hpp"

Client::Client(boost::asio::io_service& io, ClientSet& clients,
    unsigned long id,
    std::shared_ptr<boost::asio::ip::tcp::socket> tcpSocket):
clients(clients), id(id),
queue(NULL), qState(FILLING), qSize(0), qSizeMin(0), qSizeMax(0),
receivedDataNo(-1),
tcpSocket(tcpSocket),
reportTimer(io)
{
    /* turn off Nagle algorithm */
    tcpSocket->set_option(boost::asio::ip::tcp::no_delay(true));
}

Client::~Client()
{
    if (queue != NULL)
        delete[] queue;

    try {
        tcpSocket->shutdown(boost::asio::ip::tcp::socket::shutdown_both);
        tcpSocket->close();
    } catch (boost::system::system_error& e) {}
}

Client::pointer Client::create(boost::asio::io_service& io, ClientSet& clients,
    unsigned long id,
    std::shared_ptr<boost::asio::ip::tcp::socket> tcpSocket)
{
    return pointer(new Client(io, clients, id, tcpSocket));
}

void Client::start()
{
    sendId();
}

void Client::sendId()
{
    std::stringstream ss;
    ss << "CLIENT " << id << "\n";
    std::shared_ptr<std::string> message(new std::string(ss.str()));
    if (debug) std::cerr << "(TCP) " << *message;

    boost::asio::async_write(*tcpSocket, boost::asio::buffer(*message),
        boost::bind(&Client::handleSendId, shared_from_this(),
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
}

void Client::handleSendId(const boost::system::error_code& error,
    size_t /*bytes_transferred*/)
{
    if (error) {
        clients.remove(shared_from_this());
        return;
    }

    sendReport();
}

void Client::sendReport()
{
    std::string message = clients.getReport();

    boost::asio::async_write(*tcpSocket, boost::asio::buffer(message),
        boost::bind(&Client::handleSendReport, shared_from_this(),
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
}

void Client::handleSendReport(const boost::system::error_code& error,
    size_t /*bytes_transferred*/)
{
    if (error) {
        clients.remove(shared_from_this());
        return;
    }

    reportTimer.expires_from_now(std::chrono::milliseconds(REPORT_FREQ));
    reportTimer.async_wait(
        boost::bind(&Client::sendReport, shared_from_this()));

}

boost::asio::ip::tcp::socket& Client::getTcpSocket()
{
    return *tcpSocket;
}

unsigned long Client::getId()
{
    return id;
}

boost::asio::ip::udp::endpoint& Client::getUdpEndpoint()
{
    return udpEndpoint;
}

void Client::setUdpEndpoint(boost::asio::ip::udp::endpoint& endpoint)
{
    udpEndpoint = endpoint;
}

void Client::keepalive()
{
}

void Client::upload(long dataNo, char* data,
    size_t size)
{
    if (dataNo != receivedDataNo + 1)
        return;

    receivedDataNo = dataNo;
    sendAck();

    if (size == 0)
        return;

    char* old = queue;
    queue = new char[qSize + size];
    char* end = queue;

    if (old != NULL && qSize > 0)
        end = std::copy(old, old + qSize, queue);
    std::copy(data, data + size, end);

    delete[] old;

    qSize += size;
    updateStats();
    updateState();
}

void Client::updateStats()
{
    if (qSizeMin > qSize)
        qSizeMin = qSize;
    if (qSizeMax < qSize)
        qSizeMax = qSize;
}

size_t Client::getSpace()
{
    return clients.getFifoSize() - qSize;
}

size_t Client::getFifoSize()
{
    return qSize;
}

void Client::getStats(size_t& min, size_t& max)
{
    min = qSizeMin;
    max = qSizeMax;
    qSizeMin = qSize;
    qSizeMax = qSize;
}

void Client::updateState()
{
    if (qSize >= clients.getFifoHighWatermark())
        qState = ACTIVE;

    if (qSize <= clients.getFifoLowWatermark())
        qState = FILLING;
}

bool Client::isActive()
{
    return qState == ACTIVE;
}

void Client::sendAck()
{
    std::stringstream ss;
    ss << "ACK " << (receivedDataNo + 1) << " " << getSpace() << "\n";
    std::shared_ptr<std::string> message(new std::string(ss.str()));

    sendMessage(message);
}

void Client::sendMessage(std::shared_ptr<std::string> message)
{
    if (debug) std::cerr << "(UDP) sendMessage: " << *message;

    boost::asio::ip::udp::socket& socket = clients.getUdpSocket();

    socket.async_send_to(boost::asio::buffer(*message), udpEndpoint,
        boost::bind(&Client::handleSend, shared_from_this(),
            boost::asio::placeholders::error));
}

void Client::handleSend(const boost::system::error_code& error)
{
    if (error)
        clients.remove(shared_from_this());
}

void Client::updateFifo(size_t consumed)
{
    if (consumed == 0)
        return;

    char* old = queue;

    if (qSize > consumed) {
        queue = new char[qSize - consumed];
        std::copy(old + consumed, old + qSize, queue);
    } else {
        queue = NULL;
    }

    delete[] old;

    qSize -= consumed;
    updateStats();
    updateState();
}

void Client::fillMixerInput(void*& data, size_t& size)
{
    data = (void*) queue;
    if (isActive())
        size = qSize;
    else
        size = 0;
}

void Client::sendData(char* data, size_t size, long dataNo)
{
    std::stringstream ss;
    ss << "DATA " << dataNo << " " << (receivedDataNo + 1) <<  " " <<
          getSpace() << "\n";
    std::string prefix = ss.str();
    char* buffer = new char[prefix.size() + size];
    char* end;

    end = std::copy(prefix.begin(), prefix.end(), buffer);
    if (size > 0)
        std::copy(data, data + size, end);

    sendBuffer(buffer, size + prefix.size());
}

void Client::sendBuffer(char* buffer, size_t size)
{
    if (debug) std::cerr << "UDP sendBuffer: ";
    if (debug) std::cerr.write(buffer, 10);

    boost::asio::ip::udp::socket& socket = clients.getUdpSocket();
    socket.async_send_to(boost::asio::buffer(buffer, size), udpEndpoint,
        boost::bind(&Client::handleSendBuffer, shared_from_this(),
            boost::asio::placeholders::error,
            buffer));
}

void Client::handleSendBuffer(const boost::system::error_code& error,
    char* buffer)
{
    delete[] buffer;
    buffer = NULL;

    if (error)
        clients.remove(shared_from_this());
}
