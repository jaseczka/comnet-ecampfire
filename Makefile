CXX = g++
CXXFLAGS = -Wall -std=c++11 -O2
LDFLAGS = -lboost_system -lpthread
TARGETS = klient serwer

.PHONY: all clean debug

all: $(TARGETS)

debug: CXXFLAGS = -Wall -Wextra -DDEBUG -g -std=c++11
debug: all

klient: client.o tcpconnection.o udpconnection.o connectionmonitor.o
	$(CXX) $(CXXFLAGS) $^ -o $@ $(LDFLAGS)

serwer: server.o tcpserver.o udpserver.o serverclient.o clientset.o mixer.o
	$(CXX) $(CXXFLAGS) $^ -o $@ $(LDFLAGS)

tcpserver.o: tcpserver.cpp tcpserver.hpp serverclient.hpp clientset.hpp

udpserver.o: udpserver.cpp udpserver.hpp serverclient.hpp clientset.hpp

serverclient.o: serverclient.cpp serverclient.hpp clientset.hpp

clientset.o: clientset.cpp clientset.hpp serverclient.hpp mixer.hpp

mixer.o: mixer.cpp mixer.hpp

tcpconnection.o: tcpconnection.cpp tcpconnection.hpp udpconnection.hpp

udpconnection.o: udpconnection.cpp udpconnection.hpp

connectionmonitor.o: connectionmonitor.cpp connectionmonitor.hpp udpconnection.hpp tcpconnection.hpp

client.o: client.cpp tcpconnection.hpp udpconnection.hpp connectionmonitor.hpp

server.o: server.cpp tcpserver.hpp udpserver.hpp serverclient.hpp clientset.hpp mixer.hpp

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

clean:
	rm -f *.o $(TARGETS)
