#include <boost/asio.hpp>
#include <iostream>
#include <cstdint>
#include <cstdlib>
#include <unistd.h>

#include "tcpserver.hpp"
#include "udpserver.hpp"
#include "clientset.hpp"
#include "constants.hpp"

void printUsage(char* name)
{
    std::cerr << "Usage: " << name << " [options]\n" <<
        "OPTIONS:\n" <<
        "-p {PORT}\n\ttcp & udp port number; default: 16699\n" <<
        "-F {FIFO_SIZE}\n\tFIFO queue size in bytes; default: 10560\n" <<
        "-L {FIFO_LOW_WATERMARK}\n\tdefault: 0\n" <<
        "-H {FIFO_HIGH_WATERMARK}\n\tdefault: FIFO_SIZE\n" <<
        "-X {BUF_LEN}\n\toutgoing packets size in datagrams; default: 10\n" <<
        "-i {TX_INTERVAL}\n\tmixer interval in miliseconds; default: 5\n";
    exit(EXIT_FAILURE);
}


int main(int argc, char* argv[])
{
    uint16_t port = PORT_DEFAULT;
    size_t fifoSize = FIFO_SIZE_DEFAULT;
    size_t fifoLo = FIFO_LO_MARK_DEFAULT;
    size_t fifoHi = FIFO_HI_MARK_DEFAULT;
    size_t bufLen = BUF_LEN_DEFAULT;
    unsigned int interval = INTERVAL_DEFAULT;

    bool Hflag = false;

    int c;
    while ((c = getopt(argc, argv, "p:F:L:H:X:i:")) != -1) {
        switch (c) {
        case 'p':
            port = std::atoi(optarg);
            break;
        case 'F':
            fifoSize = std::atoi(optarg);
            break;
        case 'L':
            fifoLo = std::atoi(optarg);
            break;
        case 'H':
            Hflag = true;
            fifoHi = std::atoi(optarg);
            break;
        case 'X':
            bufLen = std::atoi(optarg);
            break;
        case 'i':
            interval = std::atoi(optarg);
            break;
        default:
            printUsage(argv[0]);
        }
    }

    if (!Hflag) fifoHi = fifoSize;

    if (debug) std::cerr << "PORT: " << port <<
                            " FIFO SIZE: " << fifoSize <<
                            " FIFO HI: " << fifoHi <<
                            " FIFO LO: " << fifoLo <<
                            " BUF LEN: " << bufLen <<
                            " INTERVAL: " << interval <<
                            std::endl;

    boost::asio::io_service io;

    ClientSet clients(fifoSize, fifoLo, fifoHi, bufLen);

    if (debug) std::cerr << "Tworze serwery tcp i udp..." << std::endl;

    UdpServer udpServer(io, clients, port, interval);
    TcpServer tcpServer(io, clients, port);

    if (debug) std::cerr << "Uruchamiam run..." << std::endl;

    io.run();

    exit(EXIT_FAILURE);

}
