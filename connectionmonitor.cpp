#include <boost/bind.hpp>
#include <chrono>

#include "udpconnection.hpp"
#include "tcpconnection.hpp"

#include "connectionmonitor.hpp"

ConnectionMonitor::ConnectionMonitor(boost::asio::io_service& io,
    UdpConnection& udpConnection, TcpConnection& tcpConnection):
udpConnection(udpConnection), tcpConnection(tcpConnection),
timer(io)
{
    startTimer();
}

ConnectionMonitor::~ConnectionMonitor()
{

}

void ConnectionMonitor::startTimer()
{
    timer.expires_from_now(std::chrono::milliseconds(CONN_CHECK_FREQ));
    timer.async_wait(boost::bind(&ConnectionMonitor::handleStartTimer, this));
}

void ConnectionMonitor::handleStartTimer()
{
    if (!udpConnection.isHealthy() || !tcpConnection.isHealthy()) {
        udpConnection.restart();
        tcpConnection.restart();
    }

    startTimer();
}

