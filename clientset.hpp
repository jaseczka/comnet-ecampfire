#ifndef CLIENTSET_HPP
#define CLIENTSET_HPP

#include <boost/asio.hpp>
#include <deque>
#include <utility>
#include "serverclient.hpp"
#include "constants.hpp"

struct mixer_input;

class ClientSet
{
public:
    ClientSet(size_t fifoSize, size_t fifoLo, size_t fifoHi,
        size_t bufferLength);
    virtual ~ClientSet();
    void add(unsigned long id, Client::pointer client);
    void update(unsigned long id, boost::asio::ip::udp::endpoint& endpoint);
    Client::pointer get(boost::asio::ip::udp::endpoint& endpoint);
    void remove(Client::pointer client);
    std::string getReport();
    size_t getFifoSize();
    size_t getFifoLowWatermark();
    size_t getFifoHighWatermark();
    void setUdpSocket(boost::asio::ip::udp::socket* socket);
    boost::asio::ip::udp::socket& getUdpSocket();
    void runMixer(unsigned int interval);
    void retransmit(Client::pointer client, unsigned long datagramNo);

private:
    std::map<unsigned long, Client::pointer> clientIdMap;
    std::map<boost::asio::ip::udp::endpoint, Client::pointer> clientUdpMap;
    size_t fifoSize;
    size_t fifoLo;
    size_t fifoHi;
    size_t bufferLength;
    boost::asio::ip::udp::socket* udpSocket;
    unsigned long sentDataNo;
    std::deque<std::pair<char*, size_t> > sentDataBuffer;

    void collectData(mixer_input* inputs);
    void sendData(void* outputBuffer, size_t size);
    void updateFifo(mixer_input* inputs);
    void updateMixerBuffer(void* outputBuffer, size_t outputSize);
};

#endif /* CLIENTSET_HPP */
