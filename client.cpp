#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <array>
#include <iostream>
#include <unistd.h>
#include "udpconnection.hpp"
#include "tcpconnection.hpp"
#include "connectionmonitor.hpp"
#include "constants.hpp"

void printUsage(char* name)
{
    std::cerr << "Usage: " << name << " [options] -s {SERVER_NAME}\n" <<
        "SERVER_NAME\n\tserver name or IP adress\n" <<
        "OPTIONS:\n" <<
        "-p {PORT}\n\ttcp & udp port number; default: 16699\n"
        "-X {RETRANSMIT_LIMIT}\n\tdefault: 10\n";
    exit(EXIT_FAILURE);
}

int main(int argc, char* argv[])
{
    std::string server;
    uint16_t port = PORT_DEFAULT;
    size_t retransmitLimit = RETR_LIMIT_DEFAULT;
    bool sflag = false;

    int c;
    while ((c = getopt(argc, argv, "s:p:X:")) != -1) {
        switch (c) {
        case 's':
            sflag = true;
            server = optarg;
            break;
        case 'p':
            port = std::atoi(optarg);
            break;
        case 'X':
            retransmitLimit = std::atoi(optarg);
            break;
        default:
            printUsage(argv[0]);
        }
    }

    if (!sflag) printUsage(argv[0]);

    if (debug) std::cerr << "SERVER: " << server <<
                            " PORT: " << port <<
                            " RETRANSFER LIMIT: " << retransmitLimit <<
                            std::endl;

    boost::asio::io_service io;

    UdpConnection udpConnection(io, server, port, retransmitLimit);
    TcpConnection tcpConnection(io, server, port, udpConnection);
    ConnectionMonitor connectionMonitor(io, udpConnection, tcpConnection);

    io.run();

    exit(EXIT_FAILURE);

}
