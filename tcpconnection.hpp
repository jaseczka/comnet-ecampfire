#ifndef TCPCONNECTION_HPP
#define TCPCONNECTION_HPP

#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>

#include "constants.hpp"

class TcpConnection
{
public:
    TcpConnection(boost::asio::io_service& io, std::string& server,
        uint16_t port, UdpConnection& udpConnection);
    virtual ~TcpConnection();
    bool isHealthy();
    void restart();

private:
    UdpConnection& udpConnection;
    boost::asio::io_service& io;
    std::string server;
    uint16_t port;
    boost::asio::ip::tcp::socket socket;
    boost::asio::steady_timer connectTimer;
    char buffer[TCP_BUFFER_SIZE];
    bool healthy;

    void run();
    void connect();
    void startReceive();
    void handleReceive(const boost::system::error_code& error,
        size_t bytes_transferred);
    void handleReport(size_t bytes_transferred);
    void sendId(size_t bytes_transferred);


};

#endif /* TCPCONNECTION_HPP */
