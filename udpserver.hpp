#ifndef UDPSERVER_H
#define UDPSERVER_H

#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>
#include "constants.hpp"

class ClientSet;

class UdpServer
{
public:
    UdpServer(boost::asio::io_service& io, ClientSet& clients, uint16_t port,
        unsigned int interval);
    virtual ~UdpServer();

private:
    static const std::string CLIENT;
    static const std::string UPLOAD;
    static const std::string RETRANSMIT;
    static const std::string KEEPALIVE;

    boost::asio::ip::udp::socket socket;
    boost::asio::ip::udp::endpoint remoteEndpoint;
    char buffer[UDP_BUFFER_SIZE];
    ClientSet& clients;
    boost::asio::steady_timer mixerTimer;
    unsigned int interval;

    void startReceive();
    void handleReceive(const::boost::system::error_code& error,
        size_t bytes_transferred);
    void setupClient(size_t size);
    void upload(size_t size);
    void retransmit(size_t size);
    void keepalive();
    void startMixer();
    void setupSocket(boost::asio::io_service& io, uint16_t port);
};

#endif /* UDPSERVER_H */
