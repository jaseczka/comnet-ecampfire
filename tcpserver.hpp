#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <boost/asio.hpp>

class ClientSet;

class TcpServer
{
public:
    uint16_t getPort();
    TcpServer(boost::asio::io_service& io, ClientSet& clients, uint16_t port);
    virtual ~TcpServer();

private:
    static unsigned long clientCount;

    ClientSet& clients;
    boost::asio::io_service& io_service;
    boost::asio::ip::tcp::acceptor acceptor;
    uint16_t port;

    void startAccept();
    void handleAccept(const boost::system::error_code& error,
        std::shared_ptr<boost::asio::ip::tcp::socket> socket
    );
};

#endif /* TCPSERVER_H */
