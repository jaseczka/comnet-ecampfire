#ifndef CONNECTIONMONITOR_HPP
#define CONNECTIONMONITOR_HPP

#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>
#include "constants.hpp"

class UdpConnection;
class TcpConnection;

class ConnectionMonitor
{
public:
    ConnectionMonitor(boost::asio::io_service& io,
    UdpConnection& udpConnection, TcpConnection& tcpConnection);
    virtual ~ConnectionMonitor();

private:
    UdpConnection& udpConnection;
    TcpConnection& tcpConnection;
    boost::asio::steady_timer timer;

    void startTimer();
    void handleStartTimer();
};

#endif /* CONNECTIONMONITOR_HPP */
