#include <boost/bind.hpp>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <cassert>

#include "udpconnection.hpp"

const std::string UdpConnection::DATA = "DATA ";
const std::string UdpConnection::ACK = "ACK ";

UdpConnection::UdpConnection(boost::asio::io_service& io, std::string& server,
    uint16_t port, size_t retransmitLimit):
io(io), server(server), port(port),
socket(io),
receivedDataNo(0), maxSeenDataNo(0), sentDataNo(-1),
requestedDataNo(-1), DATARequest(false),
availableSpace(0), retransmitLimit(retransmitLimit),
lastUpload(NULL), lastUploadSize(0),
keepaliveTimer(io), healthyTimer(io), monitorDataVal(-1),
input(io, dup(STDIN_FILENO)), output(io, dup(STDOUT_FILENO)),
healthy(true)
{
    run();
}

UdpConnection::~UdpConnection()
{
    if (lastUpload != NULL)
        delete[] lastUpload;
    lastUpload = NULL;
}

void UdpConnection::run()
{
    setupSocket();
    keepalive();
    monitorData();
    startReceive();
}

void UdpConnection::monitorData()
{
    healthyTimer.expires_from_now(std::chrono::milliseconds(CONN_CHECK_FREQ));
    healthyTimer.async_wait(
        boost::bind(&UdpConnection::handleMonitorData, this));

}

void UdpConnection::handleMonitorData()
{
    if (monitorDataVal == receivedDataNo) {
        if (debug) std::cerr << "(UDP) monitorData: healthy = false" <<
                                        std::endl;
        healthy = false;
        return;
    }

    monitorDataVal = receivedDataNo;
    monitorData();
}

void UdpConnection::restart()
{
    if (debug) std::cerr << "(UDP) restart" << std::endl;
    freeResources();
    run();
    healthy = true;
    if (debug) std::cerr << "(UDP) restart healthy = true" << std::endl;
}

void UdpConnection::freeResources()
{
    socket.shutdown(boost::asio::ip::udp::socket::shutdown_both);
    socket.close();
    receivedDataNo = 0;
    maxSeenDataNo = 0;
    sentDataNo = -1;
    requestedDataNo = -1;
    DATARequest = false;
    availableSpace = 0;
    if (lastUpload != NULL) {
        delete[] lastUpload;
        lastUpload = NULL;
    }
    lastUploadSize = 0;
}

void UdpConnection::setupSocket()
{
    boost::asio::ip::udp::resolver resolver(io);
    boost::asio::ip::udp::resolver::query query(server,
        std::to_string(port));
    boost::asio::ip::udp::resolver::iterator endpointIt =
        resolver.resolve(query);
    //ustawia odbiorce dla send i ogranicza nadawce dla receive
    boost::asio::connect(socket, endpointIt);
}

void UdpConnection::startReceive()
{
    socket.async_receive(boost::asio::buffer(receiveBuffer),
        boost::bind(&UdpConnection::handleReceive, this,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
}

void UdpConnection::keepalive()
{
    keepaliveTimer.expires_from_now(std::chrono::milliseconds(KEEPALIVE_FREQ));
    keepaliveTimer.async_wait(boost::bind(&UdpConnection::keepalive, this));

    std::shared_ptr<std::string> message(new std::string("KEEPALIVE\n"));

    sendMessage(message);
}

void UdpConnection::readData(size_t size)
{
    input.async_read_some(boost::asio::buffer(readBuffer, size),
        boost::bind(&UdpConnection::handleRead, this,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
}

void UdpConnection::handleRead(const boost::system::error_code& error,
    size_t bytes_transferred)
{
    if (error) {
        if (debug) std::cerr << "(UDP) handleRead: healthy = false; " <<
                        "message: " << error.message() << std::endl;
        healthy = false;
        return;
    }

    if (bytes_transferred == 0) {
        return;
    }

    std::stringstream ss;
    ss << "UPLOAD " << requestedDataNo << "\n";
    std::string prefix = ss.str();

    lastUploadSize = prefix.size() + bytes_transferred;
    if (lastUpload != NULL)
        delete[] lastUpload;
    lastUpload = new char[lastUploadSize];

    char* end = std::copy(prefix.begin(), prefix.end(), lastUpload);

    std::copy(readBuffer, readBuffer + bytes_transferred, end);
    sendLastUpload();
    ++sentDataNo;
}

void UdpConnection::handleReceive(const boost::system::error_code& error,
    size_t bytes_transferred)
{
    if (error == boost::asio::error::operation_aborted) {
        std::cerr << "(UDP) handleReceive: operation aborted" << std::endl;
        return;
    }

    if (error) {
        if (debug)
            std::cerr << "(UDP) handleReceive: healthy = false; " <<
                            "message: " << error.message() << std::endl;
        healthy = false;
        return;
    }

    if (bytes_transferred == 0) {
        startReceive();
        return;
    }

    if (DATA.compare(0, DATA.size(), receiveBuffer, DATA.size()) == 0) {
        writeData(bytes_transferred);
        trySendData();
    } else if (ACK.compare(0, ACK.size(), receiveBuffer, ACK.size()) == 0) {
        updateAck(bytes_transferred);
    } else {
        std::cerr << "Niezrozumialy komunikat (UDP)" << std::endl;
        //if (debug) std::cerr.write(receiveBuffer, bytes_transferred);
    }

    startReceive();
}

void UdpConnection::writeData(size_t size)
{
    if (debug) {
        std::cerr << "(UDP) writeData: (sentDataNo: " << sentDataNo << ")" <<
                     std::endl;
        std::cerr.write(receiveBuffer, 10);
    }

    char* endline = std::find(receiveBuffer + DATA.size(),
        receiveBuffer + size, '\n');

    if (endline == receiveBuffer + size) {
        std::cerr << "Niezrozumiay komunikat (UDP)" << std::endl;
        return;
    }

    char* rawData = endline + 1;
    size_t rawDataSize = size - (rawData - receiveBuffer);

    std::stringstream paramStream;
    paramStream.write(receiveBuffer + DATA.size(),
        endline - receiveBuffer - DATA.size());
    unsigned long dataNo;
    long ack;
    size_t space;
    paramStream >> dataNo >> ack >> space;

    if (dataNo < receivedDataNo)
        return;

    availableSpace = space;

    if (DATARequest && ack <= requestedDataNo) {
        sendLastUpload();
    }

    if (ack >= requestedDataNo) {
        DATARequest = true;
        requestedDataNo = ack;
    }

    if (dataNo > receivedDataNo + 1 &&
        dataNo <= receivedDataNo + retransmitLimit &&
        dataNo > maxSeenDataNo) {
        requestRetransmit();
        maxSeenDataNo = dataNo;
        return;
    }

    //dataNo == receivedDataNo + 1
    receivedDataNo = dataNo;
    if (dataNo > maxSeenDataNo)
        maxSeenDataNo = dataNo;

    if (rawDataSize == 0)
        return;

    char* buffer = new char[rawDataSize];
    std::copy(rawData, rawData + rawDataSize, buffer);

    boost::asio::async_write(output, boost::asio::buffer(buffer, rawDataSize),
        boost::bind(&UdpConnection::handleWrite, this, buffer));
}

void UdpConnection::handleWrite(char* buffer)
{
    delete[] buffer;
    buffer = NULL;
}

void UdpConnection::trySendData()
{
    /*if (debug) std::cerr << "(UDP) trySendData: sent " << sentDataNo <<
                            " requested " << requestedDataNo << std::endl;
    */
    if (sentDataNo + 1 != requestedDataNo)
        return;

    if (lastUpload != NULL) {
        delete[] lastUpload;
        lastUpload = NULL;
    }

    size_t size;
    if (availableSpace < STDIN_BUFFER_SIZE)
        size = availableSpace;
    else
        size = STDIN_BUFFER_SIZE;

    if (size > 0) {
        readData(size);
    }

}

void UdpConnection::requestRetransmit()
{
    if (debug) std::cerr << "(UDP) requestRetransmit" << std::endl;
    std::stringstream ss;
    ss << "RETRANSMIT " << (receivedDataNo + 1) << "\n";
    std::shared_ptr<std::string> message(new std::string(ss.str()));

    sendMessage(message);
}

void UdpConnection::updateAck(size_t size)
{
    if (debug) std::cerr << "(UDP) updateAck: " << std::endl;
    if (debug) std::cerr.write(receiveBuffer, size);

    std::stringstream paramStream;
    paramStream.write(receiveBuffer + ACK.size(), size - ACK.size());
    long ack;
    size_t space;
    paramStream >> ack >> space;

    if (ack >= sentDataNo) {
        requestedDataNo = ack;
        availableSpace = space;
        DATARequest = false;
    }
}

void UdpConnection::sendMessage(std::shared_ptr<std::string> message)
{
    socket.async_send(boost::asio::buffer(*message),
        boost::bind(&UdpConnection::handleSend, this,
            boost::asio::placeholders::error));
}

void UdpConnection::sendLastUpload()
{
    if (lastUpload == NULL)
        return;

    if (debug) std::cerr << "(UDP) sendLastUpload (" << lastUploadSize <<
                            "): " << std::endl;
    //if (debug) std::cerr.write(lastUpload, lastUploadSize);

    socket.async_send(boost::asio::buffer(lastUpload, lastUploadSize),
        boost::bind(&UdpConnection::handleSend, this,
            boost::asio::placeholders::error));
}

void UdpConnection::handleSend(const boost::system::error_code& error)
{
    if (error == boost::asio::error::operation_aborted)
        return;

    if (error) {
        if (debug) std::cerr << "(UDP) handleSend healthy = false; " <<
                            "message: " << error.message() << std::endl;
        healthy = false;
    }
}

bool UdpConnection::isHealthy()
{
    return healthy;
}
