#ifndef UDPCONNECTION_HPP
#define UDPCONNECTION_HPP

#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>
#include <memory>

#include "constants.hpp"

class UdpConnection
{
public:
    UdpConnection(boost::asio::io_service& io, std::string& server,
    uint16_t port, size_t retransmitLimit);
    virtual ~UdpConnection();
    void sendMessage(std::shared_ptr<std::string> message);
    bool isHealthy();
    void restart();

private:
    static const std::string DATA;
    static const std::string ACK;

    boost::asio::io_service& io;
    std::string& server;
    uint16_t port;
    boost::asio::ip::udp::socket socket;
    char receiveBuffer[UDP_BUFFER_SIZE];
    unsigned long receivedDataNo;
    unsigned long maxSeenDataNo;
    long sentDataNo;
    long requestedDataNo;
    bool DATARequest;
    size_t availableSpace;
    size_t retransmitLimit;
    char* lastUpload;
    size_t lastUploadSize;
    boost::asio::steady_timer keepaliveTimer;
    boost::asio::steady_timer healthyTimer;
    unsigned long monitorDataVal;
    boost::asio::posix::stream_descriptor input;
    boost::asio::posix::stream_descriptor output;
    /** Bufor do wczytywania stdin
     */
    char readBuffer[STDIN_BUFFER_SIZE];
    bool healthy;

    void setupSocket();
    void startReceive();
    void handleReceive(const boost::system::error_code& error,
        size_t bytes_transferred);
    void sendLastUpload();
    void handleSend(const boost::system::error_code& error);
    void writeData(size_t size);
    void updateAck(size_t size);
    void requestRetransmit();
    void keepalive();
    void readData(size_t size);
    void handleRead(const boost::system::error_code& error, size_t size);
    void trySendData();
    void handleWrite(char* buffer);
    void run();
    void freeResources();
    void monitorData();
    void handleMonitorData();
};

#endif /* UDPCONNECTION_HPP */
